# Project Overview
This project intends to develop a webpage allowing users to enter employee data. Once the data is entered, it will render back to the user for reference. 
The data is stored in a RDS instance on AWS. 

## Technology Stack
- terraform
- AWS 
- Gitlab

## Architecture
We have an EC2 instance running linux and a RDS instance using mysql engine. The main goal is to connect the two components in order to allow access of the information.
The below diagram explains at a high level their interaction. 

## Lessons Learned
- Became more familiar with logging into EC2 to check the connection to DB.- For my example this command will allow you to check connectivity. 
mysql -h RDSEndPoint -P 3306 -u DBName -p
- Double checking argument values provided for DB to reduce errors when creating a table. 
- I can say that I feel more comfortable working in AWS and TF without feeling  if I will break something.

## Improvements
1. Two files need to be added to the EC2. One is the HTML, and the other is a file with DB configuration information. To reduce manual intervention, find a way how to add these files without the need to add them manually.  
2. Improve security groups for RDS. Currently, inbound rule was added manually.
3. Try to add a job that can test something about our TF code.

## CICD Jobs
- validate
- test
- build
- deploy
- destroy

NOTE: These jobs are coming from template Terraform.latest.gitlab-ci.yml. 
If interested to see another example of CICD that has job to deploy code into AWS using TF, please read the resource link below. 

## References
- Example reference can be found [here](https://medium.com/geekculture/create-a-web-server-and-an-amazon-rds-database-instance-8259a352b790)
- Another example of a .gitlab-ci.yml [here](https://bmwitcher.medium.com/using-gitlab-ci-cd-to-deploy-terraform-configurations-in-aws-d13a74a6fa47)

## New ideas
During this training, several thoughts had been coming regarding my current project back at work. 
I honestly have more questions about why we did things a certain way. 
- For example, is there an easier way to create an image that can be used in AWS using docker? I need to understand more about a tool called packer vs. docker. 
- Also, the structure of our project in TF is broken down into too many modules, and it's getting more complex without really needing to be that way. 
- Did we honestly explore the best approach for our project, or did we move foward with what was given to us without questioning alternatives? 

Building infrastructure felt out of my comfort zone but now with a little more knowledge I feel comfortable asking the questions. Of course the learning continues... 

