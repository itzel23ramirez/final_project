terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

//Security Group 
resource "aws_security_group" "db-securitygroup"{
 name = "db-securitygroup"

 ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
 }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
 }

 egress {
    from_port = 0
    to_port = 0
    protocol = -1
     cidr_blocks = ["0.0.0.0/0"]
 }

 egress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "rds" {
  allocated_storage        = 10
  db_name                  = "sample"
  engine                   = "mysql"
  engine_version           = "5.7"
  instance_class           = "db.t2.micro"
  username                 = "itzel"
  password                 = "jackhoney23"
  parameter_group_name     = "default.mysql5.7"
  skip_final_snapshot  = true
}


resource "aws_instance" "ec2" {
  ami           = "ami-0dfcb1ef8550277af"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.db-securitygroup.name]
  user_data = <<EOF
  #!/bin/bash
  sudo yum update -y
  sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
  sudo yum install -y httpd
  sudo systemctl start httpd
  sudo systemctl enable httpd
  sudo usermod -a -G apache ec2-user
  EOF

  tags = {
    Name = "itzel-ec2"
  }
}
